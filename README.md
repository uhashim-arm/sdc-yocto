# SDC Yocto


This reference software stack is based on Yocto
It uses meta-ewaol which is a reference implementation of the SOAFEE architecture

This reference stack supports the following device types: 

1. Generic-arm64 (SystemReady Image)  <-  DEFAULT
2. Renesas smarc-rzg2l 
3. AWS EC2 Graviton 2 instance (generic-arm64 image with customizations)


## Setup Host Machine

1. Host machine running Ubuntu 22.04 or later
2. Following packages need to be installed 
```
sudo apt-get update
sudo apt-get install -y vim make python3 python-is-python3 git gnupg bison flex bzip2 chrpath cpio diffstat file g++ gawk wget zstd liblz4-tool kas locales xz-utils
sudo apt-get install -y libssl-dev device-tree-compiler
```
3. Set Locale to UTF-8, for example:
```
sudo locale-gen en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8
``` 
4. Setup git user
```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```
5. Clone this repository
```
git clone -b main https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-sw-stack/sdc-yocto.git
```
6. OPTIONAL: Set SSTATE_CACHE and DOWNLOADS directories appropriately in `kas_configs/sdc.yml`





## Building SystemReady image

Run the following commands: 

```
kas build kas_configs/sdc.yml  
```

Once the build completes the SystemReady Linux image can be found here: 
` build/tmp_baremetal/deploy/images/generic-arm64/ewaol-baremetal-image-generic-arm64.wic  `

This image has been tested on the following devices running SystemReady-IR firmware: 
1. Radxa Rockpi4B 
2. NXP imx8mp-evk
3. ToyBrick RK3399-ProD
4. Arm MPS3 Corstone-1000 FPGA
5. Amazon Graviton2 EC2 instance (t4g.small)


## Building for other machines

Run the following commands: 

```
kas build kas_configs/sdc.yml:kas_configs/machines/<machine-name>.yml
```

Valid machine-name options are: 
```
rzg2l
graviton2-ami
```

## Adding CSP integration/layer

```
kas build kas_configs/csp/aws.yml:kas_configs/sdc.yml
kas build kas_configs/csp/aws.yml:kas_configs/sdc.yml:kas_configs/machines/<machine-name>.yml
```



